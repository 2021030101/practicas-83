// códificar los botones
const btnCargar = document.getElementById("btnCargar");
const btnlimpiar = document.getElementById("btnLimpiar");
const input_id = document.querySelector('.inp__id');
const btn_search = document.querySelector('.search__btn');
const res = document.getElementById('lista');

function cargarDatos(id = null) {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    // realizar función de respuesa de petición
    http.onreadystatechange = function() {
        // validar respuesta
        if(this.status == 200 && this.readyState == 4) {
            const json = JSON.parse(this.responseText);
            limpiar();
             // ciclo para mostrar los datos en la tabla
            for(const datos of json) {
                if(!id) append(datos);
                if(id == datos.id) {
                    append(datos);
                    break;
                }
            }
        }
    }
    http.open('GET', url, true);
    http.send();
}

function append(datos) {
    res.innerHTML += '<tr><td class="columna1">' + datos.id + '</td>' +
                     '<td class="columna2">' + datos.name + '</td>' +
                     '<td class="columna3">' + datos.username + '</td></tr>';
}

function limpiar() {
    res.innerHTML="";
}

btnCargar.addEventListener('click', function(e) {
    cargarDatos();
});

btnlimpiar.addEventListener('click', function(e) {
    limpiar();
});

btn_search.addEventListener('click', function(e) {
    cargarDatos(input_id.value);
});