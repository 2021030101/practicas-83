const txtPais = document.querySelector('.txtPais');
const btnBuscar = document.querySelector('.btnBuscar');
const btnLimpiar = document.querySelector('.btnLimpiar');
const capital = document.querySelector('.capital > span');
const lenguaje = document.querySelector('.lenguaje > span');
const warning = document.querySelector('.warning');

let datos = null;

const limpiar = () => {
    txtPais.value = "";
    lenguaje.innerHTML = "";
    capital.innerHTML = "";
}

const fetchInfo = (name) => {
    warning.innerHTML = "";
    datos = null;
    fetch('https://restcountries.com/v3.1/name/' + name)
    .then(response => response.json())
    .then(data => mostrarInfo(data))
    .catch(error => warning.innerHTML = "Surgió un error en la busqueda.");
}

const mostrarInfo = (datos) => {
    console.log(datos);
    capital.innerHTML = datos[0].capital;
    lenguaje.innerHTML = Object.values(datos[0].languages);
}

btnBuscar.addEventListener('click', e => {
    e.preventDefault();
    fetchInfo(txtPais.value);
});

btnLimpiar.addEventListener('click', e => {
    e.preventDefault();
    limpiar();
});