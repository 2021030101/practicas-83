const razas = document.querySelector('.razas');
const warning = document.querySelector('p.warning');
const [btnCargar, btnMostrar] = document.querySelectorAll('button.inp');
const dogImage = document.querySelector('.dog__image');
const fragment = new DocumentFragment();

const cargarSelect = ({data}) => {
    const items = data.message;
    const primeraOpcion = document.createElement('option');
    primeraOpcion.value = 'false';
    primeraOpcion.textContent = 'Selecciona la Raza';
    fragment.appendChild(primeraOpcion);
    items.forEach(raza => {
        const option = document.createElement('option');
        option.value = raza;
        option.textContent = raza;
        fragment.appendChild(option);
    });
    razas.appendChild(fragment);
}

const mostrarImagen = () => {
    if(razas[razas.selectedIndex] == undefined) {
        warning.textContent = "Debes cargar primero las razas.";
        return;
    }
    if(razas[razas.selectedIndex].text == 'Selecciona la Raza') {
        warning.textContent = "Debes seleccionar una raza valida.";
        return;
    }
    warning.textContent = "";
    const cargarImagen = ({data}) => dogImage.src = data.message;
    axios.get(`https://dog.ceo/api/breed/${razas.value}/images/random`)
    .then(response => cargarImagen(response))
    .catch(err => console.log('Ocurrió un error: ' + err));
}

const axiosCall = () => {
    axios.get('https://dog.ceo/api/breeds/list')
    .then(response => cargarSelect(response))
    .catch(err => console.log('Ocurrió un error: ' + err));
}

btnCargar.addEventListener('click', e => {
    e.preventDefault();
    warning.textContent = "";
    if(razas[razas.selectedIndex] === undefined) axiosCall();
});

btnMostrar.addEventListener('click', e => {
    e.preventDefault();
    mostrarImagen();
});