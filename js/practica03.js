function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    // realizar función de respuesa de petición
    http.onreadystatechange = function() {
        // validar respuesta
        if(this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);
            // ciclo para mostrar los datos en la tabla
            for(const datos of json) {
                res.innerHTML += '<tr><td class="columna1">' + datos.id + '</td>' +
                                 '<td class="columna2">' + datos.name + '</td>' +
                                 '<td class="columna3">' + datos.username + '</td></tr>';
            }
        }
    }
    http.open('GET', url, true);
    http.send();
}

// códificar los botones
const btnCargar = document.getElementById("btnCargar");
const btnlimpiar = document.getElementById("btnLimpiar");

btnCargar.addEventListener('click', cargarDatos);

btnlimpiar.addEventListener('click', function() {
    let res = document.getElementById('lista');
    res.innerHTML="";
});