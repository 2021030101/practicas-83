const btnBuscar = document.querySelector('.btnBuscar');
const [txtID, txtNombre, txtUsuario, txtCalle, txtNumero, txtCiudad] = Array.from(document.querySelectorAll('input[type=text]'));
const txtEmail = document.querySelector('.txtEmail');
const warning = document.querySelector('.warning');

const mostrar = (respuesta, id) => {
    const user = respuesta.find(item => item.id == id);
    if(!user) return;
    txtNombre.value = user.name;
    txtUsuario.value = user.username;
    txtEmail.value = user.email;
    txtCalle.value = user.address.street;
    txtNumero.value = user.address.suite;
    txtCiudad.value = user.address.city;
}

const buscar = (id) => {
    axios.get('https://jsonplaceholder.typicode.com/users')
    .then(res => {
        warning.innerHTML = '';
        mostrar(res.data, id);
    })
    .catch(err => {
        warning.innerHTML = 'Surgió un error. Compruebe el ID.';
    });
}

btnBuscar.addEventListener('click', e => {
    e.preventDefault();
    buscar(txtID.value);
});